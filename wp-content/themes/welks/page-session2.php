<?php
/*
Template Name: 説明会（総合・一般）
*/
?>

<?php get_header("4"); ?>

<div id="main-content" class="main-content2">

	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">
	
<?php while(have_posts()): the_post(); ?>
		  
		<h2 class="re-title"><img src="/wp-content/themes/welks/images/re-session1-title.png" width="940" height="250" alt="総合職・一般職の会社説明会" /></h2>
		  
		<table class="re-table">
		
		<tr>
		<?php if( get_field('session-contents') ) { ?>
		<td class="re-td1">内容</td>
		<td class="re-td2"><?php the_field('session-contents'); ?></td>
		<?php } ?>
		</tr>
		
		<tr>
		<?php if( get_field('session-belongings') ) { ?>
		<td class="re-td1">持ち物</td>
		<td class="re-td2"><?php the_field('session-belongings'); ?></td>
		<?php } ?>
		</tr>
		
		<tr>
		<?php if( get_field('session-schedule') ) { ?>
		<td class="re-td1">日程</td>
		<td class="re-td2"><?php the_field('session-schedule'); ?></td>
		<?php } ?>
		</tr>
		  
		</table>
		
		<?php the_content(); ?>
<?php endwhile; ?>
		  
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("4"); ?>