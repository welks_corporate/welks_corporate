<?php
/*
Template Name: 下層（リニューアル）
*/
?>

<?php get_header(); ?>

<div class="bg">

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
