<h3 class="midashi3">事業所一覧</h3>

<table class="tb-simple">
    <tr>
        <td>
        <p class="txt-20 bold">本社</p><br>
        〒110-0005<br>
        東京都台東区上野3-24-6　上野フロンティアタワー13F<br><br>
		＜電話番号＞<br>
		03-6284-2901<br>
        ＜アクセス＞<br>
		銀座線「上野広小路駅」より徒歩1分<br>
		山手線「御徒町駅」より徒歩3分<br>
		大江戸線「上野御徒町駅」より徒歩4分<br>
		日比谷線「仲御徒町駅」より徒歩6分<br>
		千代田線「湯島駅」より徒歩6分<br>
		銀座線「末広町駅」より徒歩6分
        </td>
        <td><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3239.795461724596!2d139.77061615107507!3d35.70665063608695!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x60188c1ffd99c303%3A0xa4d9098226dfb807!2z44CSMTEwLTAwMDUg5p2x5Lqs6YO95Y-w5p2x5Yy65LiK6YeO77yT5LiB55uu77yS77yU4oiS77yW!5e0!3m2!1sja!2sjp!4v1513569075112" width="550" height="360" frameborder="0" style="border:0" allowfullscreen></iframe></td>
    </tr>
    <tr>
        <td>
        <p class="txt-20 bold">福岡事業所</p><br>
        〒810-0001<br>
        福岡市中央区天神4-8-2天神ビルプラス4F<br><br>
		＜電話番号＞<br>
		092-737-5097<br>
        ＜アクセス＞<br>
        地下鉄空港線「天神駅」より徒歩8分<br>
        地下鉄空港線「中洲川端駅」より徒歩9分<br>
        地下鉄七隅線「天神南駅」より徒歩11分<br>
        西日本鉄道天神大牟田線「西鉄福岡(天神)駅」より徒歩11分
        </td>
        <td><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3323.3980507757874!2d130.39817305103398!3d33.59497509904057!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3541918d7ecab75d%3A0x42f5803ec7c7bd41!2z44CSODEwLTAwMDEg56aP5bKh55yM56aP5bKh5biC5Lit5aSu5Yy65aSp56We77yU5LiB55uu77yY4oiS77ySIOWkqeelnuODk-ODq-ODl-ODqeOCuQ!5e0!3m2!1sja!2sjp!4v1500444520517" width="550" height="360" frameborder="0" style="border:0" allowfullscreen></iframe></td>
    </tr>
</table>