<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div class="bg">
	<div class="box1">

<div id="main-content" class="main-content2">
	<?php twentyfourteen_post_thumbnail(); ?>

<h2 class="singletitle"><?php the_title(); ?></h2>

	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">

<div id="topics-temp">
<p id="topics-date"><?php the_time('Y年n月j日'); ?></p>

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
</div>

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

</div>
</div>

<?php get_footer(); ?>
