<?php
/*
Template Name:ニュースリリース一覧
*/
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("2"); ?>

<div id="main-content" class="main-content2">

<h2 class="pagetitle"><?php the_title(); ?></h2>
  
	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">
		  
<div class="news2">
<ul>
<?php
    query_posts("&posts_per_page=15&cat=6,18&paged=$paged");
    if (have_posts()) :
    while ( have_posts() ) : the_post();
?>
<li><span class="date"><?php echo get_the_date(); ?></span><span class="<?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->slug; } ?>"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></span><br class="sp"><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
<?php endwhile; endif; ?>
<li><div class="pager"><?php
	global $wp_query;
	$big = 999999999;
	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages
	) );
	?>
</div></li>
<?php wp_reset_query(); ?>
</ul>
</div>
		  
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("2"); ?>