<?php
/*
Template Name: 採用情報（TOP）
*/
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("3"); ?>

<div id="main-content" class="main-content2">

<img src="<?php bloginfo('template_url'); ?>/images/re-img.jpg" alt="WELKSが社会問題解決にできること" />

	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">

<h2 class="re-title"><img src="/wp-content/themes/welks/images/re-title.png" width="412" height="38" alt="WELKS RECRUIT" /></h2>

<a href="https://woman.type.jp/s/event/?waad=Bzd9O9mL"><img src="https://welks.co.jp/wp-content/uploads/2017/06/1.png" class="aligncenter size-full wp-image-12695" alt="7月1日（土）渋谷ヒカリエにて女性のための転職イベントに参画します！" width="600" height="290">

<h3 class="topicstitle">Topics</h3>
<div class="news3">
<ul>
<?php query_posts('showposts=3&cat=15'); while(have_posts()) : the_post(); ?>
<li><span class="date"><?php echo get_the_date(); ?></span><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
<?php endwhile;?>
</ul>
</div>

<ul class="re-menu-bg">
<li class="re-menu"><a href="/recruit/newgraduates/"><img src="/wp-content/themes/welks/images/re-menu1.png" width="460" height="160" alt="新卒採用" class="fade" /></a></li><li class="re-menu"><a href="/recruit/career/"><img src="/wp-content/themes/welks/images/re-menu3.png" width="460" height="160" alt="中途採用" class="fade" /></a></li>
<li class="re-menu"><a href="/recruit/pwd/"><img src="/wp-content/themes/welks/images/re-menu2.png" width="460" height="160" alt="障がい者採用" class="fade" /></a></li><li class="re-menu"><a href="/recruit/ptj/"><img src="/wp-content/themes/welks/images/re-menu4.png" width="460" height="160" alt="パート・アルバイト" class="fade" /></a></li>
</ul>

<h3 class="recotitle">Recommend Contents</h3>
<ul class="re-menu-bg">
<li class="re-menu"><a href="/company/" target="_blank"><img src="/wp-content/themes/welks/images/re-reco1.png" width="460" height="160" alt="会社概要" class="fade" /></a></li><li class="re-menu"><a href="/recruit/message/" target="_blank"><img src="/wp-content/themes/welks/images/re-reco2.png" width="460" height="160" alt="代表メッセージ" class="fade" /></a></li><li class="re-menu"><a href="/service/" target="_blank"><img src="/wp-content/themes/welks/images/re-reco3.png" width="460" height="160" alt="事業内容" class="fade" /></a></li><li class="re-menu"><a href="/recruit/idea/" target="_blank"><img src="/wp-content/themes/welks/images/re-reco4.png" width="460" height="160" alt="人材理念・スピリット" class="fade" /></a></li><li class="re-menu"><a href="/member/" target="_blank"><img src="/wp-content/themes/welks/images/re-reco5.png" width="460" height="160" alt="社員インタビュー" class="fade" /></a></li><li class="re-menu"><a href="/recruit/system/" target="_blank"><img src="/wp-content/themes/welks/images/re-reco6.png" width="460" height="160" alt="制度・福利厚生" class="fade" /></a></li>
</ul>

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("4"); ?>
