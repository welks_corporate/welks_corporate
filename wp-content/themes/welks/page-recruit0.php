<?php
/*
Template Name: 採用情報（TOPリニューアル）
*/
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("4"); ?>

<!-- スライダー -->
<div class="swiper-container">
<div class="swiper-wrapper">

    <div class="swiper-slide"> 
        <a href="/recruit/member"><img src="<?php bloginfo('template_url'); ?>/images/slide1.jpg" class="w100 pc" alt="社員インタビュー"></a>
        <a href="/recruit/member"><img src="<?php bloginfo('template_url'); ?>/images/member/slide1-sp.png" class="w100 sp" alt="社員インタビュー"></a>
    </div>

    <div class="swiper-slide">
        <a href="/recruit/message"><img src="<?php bloginfo('template_url'); ?>/images/slide2.jpg" class="w100 pc" alt="代表メッセージ"></a>
        <a href="/recruit/message"><img src="<?php bloginfo('template_url'); ?>/images/member/slide2-sp.png" class="w100 sp" alt="代表メッセージ"></a>
    </div>
    
    <div class="swiper-slide"> 
        <a href="/recruit/system/taskforce/"><img src="<?php bloginfo('template_url'); ?>/images/slide4.jpg" class="w100 pc" alt="タスクフォース"></a>
        <a href="/recruit/system/taskforce/"><img src="<?php bloginfo('template_url'); ?>/images/slide4-sp.jpg" class="w100 sp" alt="タスクフォース"></a>
  </div>

</div>

    <div class="swiper-pagination"></div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>

</div>
<!-- スライダー -->

<div class="img-relative2">
<img src="<?php bloginfo('template_url'); ?>/images/re_reason_bg.png" alt="理由" class="w100 pc">
<img src="<?php bloginfo('template_url'); ?>/images/sp/re_reason_bg.png" alt="理由" class="w100 sp">
<h3 class="reasontitle"><img src="/wp-content/themes/welks/images/re_reasontitle.png" alt="REASON WELKSが社会問題に取り組む理由"></h3>
<p class="txt-reason">ウェルクスでは、社会問題の解決につながることのみを事業領域としています。<br>
会社の成長がダイレクトに社会への貢献度の拡大につながるため、<br class="br-pc">
社員は迷いなく全力で仕事に向き合うことができ、全力で会社を成長させていくことができます。</p>
<!-- <p class="btn-reason"><a href="/">もっと詳しく知りたい</a></p> -->
</div>

<div class="img-relative2">
<img src="<?php bloginfo('template_url'); ?>/images/re_message_bg.jpg" alt="代表メッセージ" class="w100 pc">
<img src="<?php bloginfo('template_url'); ?>/images/sp/re_message_bg.jpg" alt="代表メッセージ" class="w100 sp">
<h3 class="messagetitle pc"><img src="/wp-content/themes/welks/images/re_messagetitle.png" alt="MESSAGE 代表メッセージ"></h3>
<h3 class="messagetitle sp"><img src="/wp-content/themes/welks/images/sp/re_messagetitle.png" alt="MESSAGE 代表メッセージ"></h3>
<p class="txt-message">『プロフェッショナルなスキルを<br class="br-sp">通じて社会の問題を解決し、<br>会社に関係するすべての人々の<br class="br-sp">幸福を追求する』という<br class="br-sp">企業理念に込めた想い。<br>
代表のメッセージを<br class="br-sp">ご紹介します。</p>
  <p class="btn-message"><a href="/recruit/message/">もっと読みたい</a></p>
</div>

<h3 id="ancher-entry" class="entrytitle"><img src="/wp-content/themes/welks/images/re_entrytitle.png" alt="ENTRY"></h3>
<p class="center margin-t40">あなたらしく働ける場所が、<br class="br-sp">WELKSできっと見つかる</p>

<ul class="box-w960">
    <li>
    <a href="/recruit/newgraduates/">
       <img src="<?php bloginfo('template_url'); ?>/images/re_btn_ent_new.png" alt="新卒採用" onmouseover="this.src='<?php bloginfo('template_url'); ?>/images/re_btn_ent_new_af.png'" onmouseout="this.src='<?php bloginfo('template_url'); ?>/images/re_btn_ent_new.png'">
    </a>
    </li>
    <li>
	<a href="/recruit/career/">
       <img src="<?php bloginfo('template_url'); ?>/images/re_btn_ent_car.png" alt="中途採用" onmouseover="this.src='<?php bloginfo('template_url'); ?>/images/re_btn_ent_car_af.png'" onmouseout="this.src='<?php bloginfo('template_url'); ?>/images/re_btn_ent_car.png'">
    </a>
	    </li>
</ul>

<ul class="box-w960b">
    <li>
    <a href="/recruit/pwd/">
       <img src="<?php bloginfo('template_url'); ?>/images/re_btn_ent_pwd.png" alt="障害者採用" onmouseover="this.src='<?php bloginfo('template_url'); ?>/images/re_btn_ent_pwd_af.png'" onmouseout="this.src='<?php bloginfo('template_url'); ?>/images/re_btn_ent_pwd.png'">
    </a>
    </li>
    <li>
    <a href="/recruit/ptj/">
       <img src="<?php bloginfo('template_url'); ?>/images/re_btn_ent_ptj.png" alt="パート・アルバイト" onmouseover="this.src='<?php bloginfo('template_url'); ?>/images/re_btn_ent_ptj_af.png'" onmouseout="this.src='<?php bloginfo('template_url'); ?>/images/re_btn_ent_ptj.png'">
    </a>
</ul>

<hr class="hr-1bk">

<h3 class="recotitle1 margin-b20"><img src="/wp-content/themes/welks/images/re_recotitle.png" alt="RECOMEND CONTENTS"></h3>
<ul class="box-w960">
      <li>
    <a href="/recruit/company/">
       <img src="<?php bloginfo('template_url'); ?>/images/re_reco_company.png" alt="会社情報" onmouseover="this.src='<?php bloginfo('template_url'); ?>/images/re_reco_company_af.png'" onmouseout="this.src='<?php bloginfo('template_url'); ?>/images/re_reco_company.png'">
    </a>
    </li>
    <li>
    <a href="/recruit/member/">
       <img src="<?php bloginfo('template_url'); ?>/images/re_reco_intervew.png" alt="社員インタビュー" onmouseover="this.src='<?php bloginfo('template_url'); ?>/images/re_reco_intervew_af.png'" onmouseout="this.src='<?php bloginfo('template_url'); ?>/images/re_reco_intervew.png'">
    </a>
    </li>
      <li>
    <a href="/recruit/data/">
       <img src="<?php bloginfo('template_url'); ?>/images/re_reco_data.png" alt="データでみるウェルクス" onmouseover="this.src='<?php bloginfo('template_url'); ?>/images/re_reco_data_af.png'" onmouseout="this.src='<?php bloginfo('template_url'); ?>/images/re_reco_data.png'">
    </a>
    </li>
    <li>
	<a href="/recruit/system/taskforce/">
       <img src="<?php bloginfo('template_url'); ?>/images/re_reco_taskforce.png" alt="タスクフォース" onmouseover="this.src='<?php bloginfo('template_url'); ?>/images/re_reco_taskforce_af.png'" onmouseout="this.src='<?php bloginfo('template_url'); ?>/images/re_reco_taskforce.png'">
    </a>
    </li>
</ul>		  
<h3 class="newstitle"><img src="/wp-content/themes/welks/images/re_newstitle.png" alt="NEWS 新着情報"></h3>
<div class="news4">
<ul>
<?php query_posts('showposts=3&cat=15'); while(have_posts()) : the_post(); ?>
<li><span class="date"><?php echo get_the_date(); ?></span><a href="<?php the_permalink();?>"><?php the_title();?></a></li>
<?php endwhile;?>
</ul>
</div>
  
<?php get_footer("4"); ?>