<?php
/*
Template Name:下層ページ
*/
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("2"); ?>

<div id="main-content" class="main-content2">

<h2 class="pagetitle"><?php the_title(); ?></h2>
  
	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">
		  
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
		  
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("2"); ?>