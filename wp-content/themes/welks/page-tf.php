<?php
/*
Template Name:TFページ
*/
?>

<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon" href="https://welks.co.jp/wp-content/themes/welks/images/favicon.ico" type="image/ico" />
<link rel="profile" href="https://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/wp-content/themes/welks/css/swiper.css">
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
<script>
jQuery(function(){
  jQuery("#toggle").click(function(){
    jQuery("#hmenu").slideToggle();
    return false;
  });
  jQuery(window).resize(function(){
    var win = $(window).width();
    var p = 768;
    if(win > p){
      jQuery("#hmenu").show();
    } else {
      jQuery("#hmenu").hide();
    }
  });
});
</script>
<!-- B-Dash Tag -->
<script type="text/javascript" src="//analytics.fs-bdash.com/5A3T0E/bd-5A3T0E-1.js" charset="UTF-8"></script>
<!-- End B-Dash Tag -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '305178122992284');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=305178122992284&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>
<body class="bodybg">
<div id="page1" class="site2">
	<?php if ( get_header_image() ) : ?>
	<header id="masthead" class="site-header" role="banner">
	  	  <div class="header-main3">
		<div class="site-title2">
		<h1 class="logo2"><a href="/recruit/"><img src="<?php bloginfo('template_url'); ?>/images/header-logo2.png" width="220" height="26" alt="株式会社WELKS採用サイト" /></a></h1>
	<ul id="hmenu2">
	    <li><a href="/recruit/message/">代表メッセージ</a></li>
	    <li><a href="/recruit/member/">社員紹介</a></li>
	    <li><a href="/recruit/idea">人材理念</a></li>
	    <li><a href="/recruit/system/">社内制度</a></li>
	    <li><a href="/recruit/company/">会社情報</a></li>
	</ul>
		  <p class="hmenu2-entry"><a href="/recruit2/#ancher-entry">ENTRY</a></p>
		  <div id="toggle"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/sp/bt-menu.png" width="26" height="22" /></a></div>
		  </div>
	<ul id="hmenu">
	    <li><a href="/recruit/message/">代表メッセージ</a></li>
	    <li><a href="/recruit/member/">社員紹介</a></li>
	    <li><a href="/recruit/idea">人材理念</a></li>
	    <li><a href="/recruit/system/">社内制度</a></li>
	    <li><a href="/recruit/company/">会社情報</a></li>
	    <li><a href="/recruit2/#ancher-entry">ENTRY</a></li>
    </ul>
		</div>
	</header><!-- #masthead -->
	<?php endif; ?>

	<div id="main1" class="site-main">

<div id="main-content" class="main-content2">

<h2 class="pagetitle2">タスクフォース</h2>

	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">

<?php if(have_posts()): while(have_posts()): the_post(); ?>

		<?php if( get_field('tf-img') ) { ?>
	  	<?php $imgid = get_field('tf-img');
		$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
	  	<div class="tf-img"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></div>
		<?php } ?>

		<p class="txt-28 txt-skyblue margin-t30">ミッション</p>
		<?php if( get_field('tf-mission') ) { ?>
		<p><?php the_field('tf-mission'); ?></p>
		<?php } ?>

		<p class="txt-28 txt-skyblue margin-t30">取り組んでいること</p>
		<?php if( get_field('tf-efforts') ) { ?>
		<p><?php the_field('tf-efforts'); ?></p>
		<?php } ?>

		<table class="tf-subimg-tb margin-t30">
		    <tr>
			    <td class="tf-subimg-td1">
		<?php if( get_field('tf-subimg1') ) { ?>
	  	<?php $imgid = get_field('tf-subimg1');
		$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
		<img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>">
	    <?php } ?>
			    </td>
			    <td class="tf-subimg-td2">
		<?php if( get_field('tf-subimg2') ) { ?>
	  	<?php $imgid = get_field('tf-subimg2');
		$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
		<img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>">
        <?php } ?>
			  </td>
		    </tr>
		</table>

<div id="ft-list">
<ul>
 	<li><a href="/taskforce/cost/">コスト削減</a></li>
 	<li><a href="/taskforce/communication/">社内交流推進</a></li>
 	<li><a href="/taskforce/referral/">リファラルリクルーティング</a></li>
 	<li><a href="/taskforce/exercise/">運動増進</a></li>
 	<li><a href="/taskforce/itskill/">ITスキル</a></li>
 	<li><a href="/taskforce/partnership/">PS向上</a></li>
 	<li><a href="/taskforce/spirit/">スピリット浸透</a></li>
 	<li><a href="/taskforce/moving/">移転準備</a></li>
 	<li><a href="/taskforce/newbusiness/">新規事業開発</a></li>
 	<li><a href="/taskforce/knowhow/">ノウハウ共有</a></li>
 	<li><a href="/taskforce/strategy/">理念・戦略</a></li>
</ul>
</div>

<?php the_content(); ?>

<?php endwhile; endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("4"); ?>
