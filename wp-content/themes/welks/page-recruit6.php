<?php
/*
Template Name: 採用情報（会社情報）
*/
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("4"); ?>

<div id="main-content" class="main-content2">
  
<?php if( wp_is_mobile()) : ?>
<img src="/wp-content/themes/welks/images/re-com-main.jpg" alt="会社情報" />
<?php else : ?>
<img src="/wp-content/themes/welks/images/re-com-main.jpg" alt="会社情報" />
<?php endif; ?>
  
	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">

<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
		  
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("4"); ?>