<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon" href="https://welks.co.jp/wp-content/themes/welks/images/favicon.ico" type="image/ico" />
<link rel="profile" href="https://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/tab.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
<script>
jQuery(function(){
  jQuery("#toggle").click(function(){
    jQuery("#hmenu").slideToggle();
    return false;
  });
  jQuery(window).resize(function(){
    var win = $(window).width();
    var p = 768;
    if(win > p){
      jQuery("#hmenu").show();
    } else {
      jQuery("#hmenu").hide();
    }
  });
});
</script>
<script>
jQuery(function() {
    function accordion() {
        jQuery(this).toggleClass("active").next().slideToggle(300);
    }
    jQuery(".com-menu-sp .com-toggle").click(accordion);
});
</script>
<script>
jQuery(function(){
	jQuery('.tabbox:first').show();
	jQuery('#tab li:first').addClass('active');
	jQuery('#tab li').click(function() {
		jQuery('#tab li').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.tabbox').hide();
		jQuery(jQuery(this).find('a').attr('href')).fadeIn();
		return false;
	});
});
</script>
<script>
jQuery(function(){
	jQuery('.tabbox-tab:first').show();
	jQuery('#tab-tab li:first').addClass('active');
	jQuery('#tab-tab li').click(function() {
		jQuery('#tab-tab li').removeClass('active');
		jQuery(this).addClass('active');
		jQuery('.tabbox-tab').hide();
		jQuery(jQuery(this).find('a').attr('href')).fadeIn();
		return false;
	});
});
</script>
<script>
jQuery(function(){
    jQuery( '#mw_wp_form_mw-wp-form-1257 select option[value=""], #mw_wp_form_mw-wp-form-1316 select option[value=""]' )
        .html( '選択してください' );
} );
</script>
<!-- B-Dash Tag -->
<script type="text/javascript" src="//analytics.fs-bdash.com/5A3T0E/bd-5A3T0E-1.js" charset="UTF-8"></script>
<!-- End B-Dash Tag -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '305178122992284');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=305178122992284&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
</head>
<body class="bodybg">
<div id="page1" class="site2">
	<?php if ( get_header_image() ) : ?>
	<header id="masthead" class="site-header" role="banner">
	  	  <div class="header-main2">
		<div class="site-title2">
		<h1 class="logo"><a href="/"><img src="<?php bloginfo('template_url'); ?>/images/header-logo.png" width="133" height="26" alt="株式会社WELKS" /></a></h1>
	<ul id="hmenu3-sns0">
	<li><a href="https://www.wantedly.com/companies/welks/"><img src="<?php bloginfo('template_url'); ?>/images/logo_wantedly.png" width="22" height="22"></a></li>
	<li><a href="https://www.pr-table.com/welks/"><img src="<?php bloginfo('template_url'); ?>/images/logo_pr.png" width="22" height="22"></a></li>
	<li><a href="https://www.facebook.com/welks423/"><img src="<?php bloginfo('template_url'); ?>/images/logo_fb.png" width="22" height="22"></a></li>
	</ul>
  <ul id="hmenu1">
    <li><a href="/service/">事業内容</a></li>
    <li><a href="/company/">会社情報</a></li>
    <li><a href="/recruit/" target="_blank">採用情報</a></li>
    <li><a href="/form/">お問い合わせ</a></li>
	<li><a href="https://www.wantedly.com/companies/welks/"><img src="<?php bloginfo('template_url'); ?>/images/logo_wantedly.png" width="22" height="22"></a></li>
	<li><a href="https://www.pr-table.com/welks/"><img src="<?php bloginfo('template_url'); ?>/images/logo_pr.png" width="22" height="22"></a></li>
	<li><a href="https://www.facebook.com/welks423/"><img src="<?php bloginfo('template_url'); ?>/images/logo_fb.png" width="22" height="22"></a></li>
  </ul>
		  <div id="toggle"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/sp/bt-menu.png" width="26" height="22" /></a></div>
		  </div>
	<ul id="hmenu">
    <li><a href="/service/">事業内容</a></li>
    <li><a href="/company/">会社情報</a></li>
    <li><a href="/recruit/" target="_blank">採用情報</a></li>
    <li><a href="/form/">お問い合わせ</a></li>
    <li><a href="/privacy/">個人情報保護方針</a></li>
    <li><a href="/questionnaire/">アンケート調査実施に関する規約</a></li>
    <li><a href="/sitemap/">サイトマップ</a></li>
  </ul>
		</div>
	</header><!-- #masthead -->
	<?php endif; ?>
	<div id="main1" class="site-main">
