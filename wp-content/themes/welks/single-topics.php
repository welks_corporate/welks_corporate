<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("2"); ?>

<div id="main-content" class="main-content2">
	<?php twentyfourteen_post_thumbnail(); ?>

<h2 class="singletitle"><?php the_title(); ?></h2>

	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">

<div id="topics-temp">
<p id="topics-date"><?php the_time('Y年n月j日'); ?></p>
	  <div class="share_btn">
		<ul class="share_btn">
		  <li class="twitter"><a class="twitter_btn" href="https://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fa fa-twitter fw"></i><span class="text">ツイート</span><?php social_twitter(); ?></a></li>
		  <li class="facebook"><a class="facebook_btn" href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><i class="fa fa-facebook fw"></i><span class="text">シェア</span><?php social_facebook(); ?></a></li>
		  <li class="google"><a class="google" href="javascript:(function(){window.open('https://plusone.google.com/_/+1/confirm?hl=ja&url=<?php echo get_permalink() ?>'+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title),'_blank');})();"><i class="fa fa-google-plus fw"></i><span class="text">共有</span><?php social_google(); ?></a></li>
		  <li class="hatebu"><a class="hatebu_btn" href="https://b.hatena.ne.jp/add?mode=confirm&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"><span class="hatebu_icon">B!</span><span class="hatebu_chara">はてブ</span></a></li>
		  <li class="line"><a class="line" href="https://line.naver.jp/R/msg/text/?<?php the_title(); ?>%0D%0A<?php the_permalink(); ?>?action=line"><img src="/img/common/linebutton.png" /><span class="text">送る</span></a></li>
		  <li class="pocket"><a href="https://getpocket.com/edit?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" onclick="window.open(this.href, 'FBwindow', 'width=550, height=350, menubar=no, toolbar=no, scrollbars=yes'); return false;"><i class="fa fa-angle-down fw"></i><span class="text">あとで</span></a></li>
		</ul>
	  </div>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
</div>

<div class="fb-plugin">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.6";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-page" data-href="https://www.facebook.com/welks423/" data-tabs="timeline" data-width="500" data-height="280" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/welks423/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/welks423/">株式会社ウェルクス</a></blockquote></div>
</div><!-- .fb-plugin -->

		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("2"); ?>
