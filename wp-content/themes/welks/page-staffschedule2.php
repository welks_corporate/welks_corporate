<?php
/*
Template Name:社員の1日（時短CA）
*/
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("4"); ?>

<div id="main-content" class="main-content2">
  
	<div class="content-area3">
		<div class="site-content">
		  
<?php if(have_posts()): while(have_posts()): the_post(); ?>
		  
				<?php if( get_field('tl-titleimg') ) { ?>
				<?php $imgid = get_field('tl-titleimg');
				$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
				    <p><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></p>
				<?php } ?>  
		  
	<section id="mem-tl-jitanca">

		<div class="mem-tl-block">
			<div class="mem-tl-jitanca">
			</div> <!-- cd-timeline-img -->

		<div class="mem-tl-block">
			<div class="mem-tl-content">
				<h2 class="mem-tl-h2">
				    <?php if( get_field('tl-time1') ) { ?>
				        <span class="mem-tl-txt-jitanca"><?php the_field('tl-time1'); ?></span>
				    <?php } ?>
				    <?php if( get_field('tl-title1') ) { ?>
				        <?php the_field('tl-title1'); ?>
				    <?php } ?>				  
				</h2>
				    <?php if( get_field('tl-text1') ) { ?>
				        <p><?php the_field('tl-text1'); ?></p>
				    <?php } ?>
			</div> <!-- cd-timeline-content -->
		</div> <!-- mem-tl-block -->
	    </div>
	  

		<div class="mem-tl-block">
			<div class="mem-tl-jitanca">
			</div> <!-- cd-timeline-img -->

		<div class="mem-tl-block">
			<div class="mem-tl-content">
				<h2 class="mem-tl-h2">
				    <?php if( get_field('tl-time2') ) { ?>
				        <span class="mem-tl-txt-jitanca"><?php the_field('tl-time2'); ?></span>
				    <?php } ?>
				    <?php if( get_field('tl-title2') ) { ?>
				        <?php the_field('tl-title2'); ?>
				    <?php } ?>				  
				</h2>
				    <?php if( get_field('tl-text2') ) { ?>
				        <p><?php the_field('tl-text2'); ?></p>
				    <?php } ?>
			</div> <!-- cd-timeline-content -->
		</div> <!-- mem-tl-block -->
	    </div>
	  
	  
	  
		<div class="mem-tl-block">
			<div class="mem-tl-jitanca">
			</div> <!-- cd-timeline-img -->

		<div class="mem-tl-block">
				<?php if( get_field('tl-img1') ) { ?>
				<?php $imgid = get_field('tl-img1');
				$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
				    <p class="mem-tl-img-pc"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></p>
				<?php } ?>
			<div class="mem-tl-content">
				<h2 class="mem-tl-h2">
				    <?php if( get_field('tl-time3') ) { ?>
				        <span class="mem-tl-txt-jitanca"><?php the_field('tl-time3'); ?></span>
				    <?php } ?>
				    <?php if( get_field('tl-title3') ) { ?>
				        <?php the_field('tl-title3'); ?>
				    <?php } ?>				  
				</h2>
				    <?php if( get_field('tl-text3') ) { ?>
				        <p><?php the_field('tl-text3'); ?></p>
				    <?php } ?>
			</div> <!-- cd-timeline-content -->
				<?php if( get_field('tl-img1') ) { ?>
				<?php $imgid = get_field('tl-img1');
				$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
				    <p class="mem-tl-img-sp"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></p>
				<?php } ?>
		</div> <!-- mem-tl-block -->
	    </div>
	  
	  
		<div class="mem-tl-block">
			<div class="mem-tl-jitanca">
			</div> <!-- cd-timeline-img -->

		<div class="mem-tl-block">
			<div class="mem-tl-content">
				<h2 class="mem-tl-h2">
				    <?php if( get_field('tl-time4') ) { ?>
				        <span class="mem-tl-txt-jitanca"><?php the_field('tl-time4'); ?></span>
				    <?php } ?>
				    <?php if( get_field('tl-title4') ) { ?>
				        <?php the_field('tl-title4'); ?>
				    <?php } ?>				  
				</h2>
				    <?php if( get_field('tl-text4') ) { ?>
				        <p><?php the_field('tl-text4'); ?></p>
				    <?php } ?>
			</div> <!-- cd-timeline-content -->
		</div> <!-- mem-tl-block -->
	    </div>


		<div class="mem-tl-block">
			<div class="mem-tl-jitanca">
			</div> <!-- cd-timeline-img -->

		<div class="mem-tl-block">
			<div class="mem-tl-content">
				<h2 class="mem-tl-h2">
				    <?php if( get_field('tl-time5') ) { ?>
				        <span class="mem-tl-txt-jitanca"><?php the_field('tl-time5'); ?></span>
				    <?php } ?>
				    <?php if( get_field('tl-title5') ) { ?>
				        <?php the_field('tl-title5'); ?>
				    <?php } ?>				  
				</h2>
				    <?php if( get_field('tl-text5') ) { ?>
				        <p><?php the_field('tl-text5'); ?></p>
				    <?php } ?>
			</div> <!-- cd-timeline-content -->
		</div> <!-- mem-tl-block -->
	    </div>
	  

		<div class="mem-tl-block">
			<div class="mem-tl-jitanca">
			</div> <!-- cd-timeline-img -->

		<div class="mem-tl-block">
			<div class="mem-tl-content">
				<h2 class="mem-tl-h2">
				    <?php if( get_field('tl-time6') ) { ?>
				        <span class="mem-tl-txt-jitanca"><?php the_field('tl-time6'); ?></span>
				    <?php } ?>
				    <?php if( get_field('tl-title6') ) { ?>
				        <?php the_field('tl-title6'); ?>
				    <?php } ?>				  
				</h2>
				    <?php if( get_field('tl-text6') ) { ?>
				        <p><?php the_field('tl-text6'); ?></p>
				    <?php } ?>
			</div> <!-- cd-timeline-content -->
		</div> <!-- mem-tl-block -->
	    </div>
	  

		<div class="mem-tl-block">
			<div class="mem-tl-jitanca">
			</div> <!-- cd-timeline-img -->

		<div class="mem-tl-block">
			<div class="mem-tl-content">
				<h2 class="mem-tl-h2">
				    <?php if( get_field('tl-time7') ) { ?>
				        <span class="mem-tl-txt-jitanca"><?php the_field('tl-time7'); ?></span>
				    <?php } ?>
				    <?php if( get_field('tl-title7') ) { ?>
				        <?php the_field('tl-title7'); ?>
				    <?php } ?>				  
				</h2>
				    <?php if( get_field('tl-text7') ) { ?>
				        <p><?php the_field('tl-text7'); ?></p>
				    <?php } ?>
			</div> <!-- cd-timeline-content -->
		</div> <!-- mem-tl-block -->
	    </div>
	  

		<div class="mem-tl-block0">
			<div class="mem-tl-jitanca">
			</div> <!-- cd-timeline-img -->

		<div class="mem-tl-block">
				<?php if( get_field('tl-img2') ) { ?>
				<?php $imgid = get_field('tl-img2');
				$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
				    <p class="mem-tl-img-pc"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></p>
				<?php } ?>
			<div class="mem-tl-content">
				<h2 class="mem-tl-h2">
				    <?php if( get_field('tl-time8') ) { ?>
				        <span class="mem-tl-txt-jitanca"><?php the_field('tl-time8'); ?></span>
				    <?php } ?>
				    <?php if( get_field('tl-title8') ) { ?>
				        <?php the_field('tl-title8'); ?>
				    <?php } ?>				  
				</h2>
				    <?php if( get_field('tl-text8') ) { ?>
				        <p><?php the_field('tl-text8'); ?></p>
				    <?php } ?>
			</div> <!-- cd-timeline-content -->
				<?php if( get_field('tl-img2') ) { ?>
				<?php $imgid = get_field('tl-img2');
				$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
				    <p class="mem-tl-img-sp"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></p>
				<?php } ?>
		</div> <!-- mem-tl-block -->
	    </div>
	  
	</section>
		  
		</div>
	</div>
  
	<div class="content-area2">
		<div class="site-content">

<h3 id="ancher-entry" class="entrytitle"><img src="/wp-content/themes/welks/images/re_entrytitle.png" alt="ENTRY"></h3>
<p class="center margin-t40">あなたらしく働ける場所が、<br class="br-sp">WELKSできっと見つかる</p>

<ul class="box-w960">
    <li>
    <a href="/recruit/newgraduates/"><img src="/wp-content/themes/welks/images/re_btn_ent_new.png" alt="新卒採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_new_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_new.png'"></a>
    </li>
    <li>
	<a href="/recruit/career/"><img src="/wp-content/themes/welks/images/re_btn_ent_car.png" alt="中途採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_car_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_car.png'"></a>
	    </li>
</ul>

<ul class="box-w960b">
    <li>
    <a href="/recruit/pwd/"><img src="/wp-content/themes/welks/images/re_btn_ent_pwd.png" alt="障害者採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_pwd_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_pwd.png'"></a>
    </li>
    <li>
    <a href="/recruit/ptj/"><img src="/wp-content/themes/welks/images/re_btn_ent_ptj.png" alt="パート・アルバイト" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_ptj_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_ptj.png'"></a>
</ul>
		  
<?php the_content(); ?>
    <div class="prtable"><iframe frameborder="0" style="width:100%;height:500px;" src="https://www.pr-table.com/embed/welks"></iframe></div>
<?php endwhile; endif; ?>
		  
		</div>
	</div>
  
</div><!-- #main-content -->

<?php get_footer("4"); ?>