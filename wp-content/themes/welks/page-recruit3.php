<?php
/*
Template Name: 採用情報（詳細）
*/
?>

<?php get_header("4"); ?>

<div id="main-content" class="main-content2">

	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">
	
<?php while(have_posts()): the_post(); ?>
		 
		<h2 class="re-shokushu"><?php the_title(); ?></h2>
		  
		<?php if( get_field('re-mainimg') ) { ?>
	  	<?php $imgid = get_field('re-mainimg');
		$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
	  	<img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>">
		<?php } ?>
		
		<?php if( get_field('re-introtitle') ) { ?>
		<p class="re-introtitle"><?php the_field('re-introtitle'); ?></p>
		<?php } ?>
		
		<?php if( get_field('intro-jc1') ) { ?>
		<p><?php the_field('intro-jc1'); ?></p>
		<?php } ?>
		
		<h3 class="re-subtitle">仕事内容</h3>
		  
		<?php if( get_field('intro-jc2') ) { ?>
		<p><?php the_field('intro-jc2'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('re-html') ) { ?>
		<p><?php the_field('re-html'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('jc-flow') ) { ?>
		<p class="re-jctitle1"><?php the_field('jc-flow'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('flow1-1') ) { ?>
		<p class="re-jctitle2"><?php the_field('flow1-1'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('flow1-2') ) { ?>
		<p><?php the_field('flow1-2'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('flow2-1') ) { ?>
		<p class="re-jctitle2"><?php the_field('flow2-1'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('flow2-2') ) { ?>
		<p><?php the_field('flow2-2'); ?></p>
		<?php } ?>

		<?php if( get_field('flow3-1') ) { ?>
		<p class="re-jctitle2"><?php the_field('flow3-1'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('flow3-2') ) { ?>
		<p><?php the_field('flow3-2'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('flow-annotation') ) { ?>
		<p><?php the_field('flow-annotation'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('jc-title1') ) { ?>
		<p class="re-jctitle1"><?php the_field('jc-title1'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('jc-text1') ) { ?>
		<p><?php the_field('jc-text1'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('jc-title2') ) { ?>
		<p class="re-jctitle1"><?php the_field('jc-title2'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('jc-text2') ) { ?>
		<p><?php the_field('jc-text2'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('jc-title3') ) { ?>
		<p class="re-jctitle1"><?php the_field('jc-title3'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('jc-text3') ) { ?>
		<p><?php the_field('jc-text3'); ?></p>
		<?php } ?>
		  
		<h3 class="re-subtitle">募集要項</h3>
		  
		<table class="re-table">
		
		<tr>
		<?php if( get_field('job-category') ) { ?>
		<td class="re-td1">職種</td>
		<td class="re-td2"><?php the_field('job-category'); ?></td>
		<?php } ?>
		</tr>
		
		<tr>
		<?php if( get_field('employment-status') ) { ?>
		<td class="re-td1">雇用形態</td>
		<td class="re-td2"><?php the_field('employment-status'); ?></td>
		<?php } ?>
		</tr>
		
		<tr>
		<?php if( get_field('payroll') ) { ?>
		<td class="re-td1">給与</td>
		<td class="re-td2"><?php the_field('payroll'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('transportation-cost') ) { ?>
		<td class="re-td1">交通費</td>
		<td class="re-td2"><?php the_field('transportation-cost'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('location') ) { ?>
		<td class="re-td1">勤務地</td>
		<td class="re-td2"><?php the_field('location'); ?></td>
		<?php } ?>
		</tr>
		 
		<tr>
		<?php if( get_field('hours') ) { ?>
		<td class="re-td1">勤務時間</td>
		<td class="re-td2"><?php the_field('hours'); ?></td>
		<?php } ?>
	 	</tr>
		  
		<tr>
		<?php if( get_field('holiday') ) { ?>
		<td class="re-td1">休日・休暇</td>
		<td class="re-td2"><?php the_field('holiday'); ?></td>
		<?php } ?>
  		</tr>
		  
		<tr>
		<?php if( get_field('welfare') ) { ?>
		<td class="re-td1">福利厚生・待遇</td>
		<td class="re-td2"><?php the_field('welfare'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('required-skill') ) { ?>
		<td class="re-td1">必須スキル</td>
		<td class="re-td2"><?php the_field('required-skill'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('welcome-skills') ) { ?>
		<td class="re-td1">歓迎スキル</td>
		<td class="re-td2"><?php the_field('welcome-skills'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('status') ) { ?>
		<td class="re-td1">求める人物像</td>
		<td class="re-td2"><?php the_field('status'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('training-period') ) { ?>
		<td class="re-td1">研修期間</td>
		<td class="re-td2"><?php the_field('training-period'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('selection-process') ) { ?>
		<td class="re-td1">選考プロセス</td>
		<td class="re-td2"><?php the_field('selection-process'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('qualification-requirements') ) { ?>
		<td class="re-td1">応募資格</td>
		<td class="re-td2"><?php the_field('qualification-requirements'); ?></td>
		<?php } ?>
		</tr>
		  
		<tr>
		<?php if( get_field('re-memo') ) { ?>
		<td class="re-td1">備考</td>
		<td class="re-td2"><?php the_field('re-memo'); ?></td>
		<?php } ?>
		</tr>
		  
		</table>
		 
		<?php the_content(); ?>
<?php endwhile; ?>
		  
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("4"); ?>