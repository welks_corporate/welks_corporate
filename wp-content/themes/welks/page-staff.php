<?php
/*
Template Name:社員紹介ページ
*/
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("4"); ?>

<div id="main-content" class="main-content2">
  
	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">
		  
<?php if(have_posts()): while(have_posts()): the_post(); ?>
		  
		<div id="mem-box">
		  
		<?php if( get_field('member-img') ) { ?>
	  	<?php $imgid = get_field('member-img');
		$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
	  	<div class="mem-img"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></div>
		<?php } ?>
		
		<?php if( get_field('member-name') ) { ?>
		<p class="mem-name"><?php the_field('member-name'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-q1') ) { ?>
		<p class="mem-q"><?php the_field('member-q1'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-a1') ) { ?>
		<p><?php the_field('member-a1'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-q2') ) { ?>
		<p class="mem-q"><?php the_field('member-q2'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-a2') ) { ?>
		<p><?php the_field('member-a2'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-subimg1') ) { ?>
	  	<?php $imgid = get_field('member-subimg1');
		$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
	  	<div class="mem-subimg"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></div>
		<?php } ?>
		  
		<?php if( get_field('member-q3') ) { ?>
		<p class="mem-q"><?php the_field('member-q3'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-a3') ) { ?>
		<p><?php the_field('member-a3'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-q4') ) { ?>
		<p class="mem-q"><?php the_field('member-q4'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-a4') ) { ?>
		<p><?php the_field('member-a4'); ?></p>
		<?php } ?>
		  
		<div class="mem-sche">
			<?php $imgid = get_field('member-sche-title'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
		<p class="mem-sche-title"><?php the_field('member-sche-title',$post->ID); ?></p>
			<?php endif;?>
		  
			<?php $imgid = get_field('member-sche1-1'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
		  	<table class="mem-tb">
		    <tr>
			<td class="memtd1">
			<p><?php the_field('member-sche1-1',$post->ID); ?></p>
			</td><?php endif;?>
			<?php $imgid = get_field('member-sche1-2'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td>
			<p><?php the_field('member-sche1-2',$post->ID); ?></p>
			</td><?php endif;?>
		</tr>
		<tr>
			<?php $imgid = get_field('member-sche2-1'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td class="memtd1">
			<p><?php the_field('member-sche2-1',$post->ID); ?></p>
			</td><?php endif;?>
			<?php $imgid = get_field('member-sche2-2'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td>
			<p><?php the_field('member-sche2-2',$post->ID); ?></p>
			</td><?php endif;?>
		</tr>
		<tr>
			<?php $imgid = get_field('member-sche3-1'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td class="memtd1">
			<p><?php the_field('member-sche3-1',$post->ID); ?></p>
			</td><?php endif;?>
			<?php $imgid = get_field('member-sche3-2'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td>
			<p><?php the_field('member-sche3-2',$post->ID); ?></p>
			</td><?php endif;?>
		</tr>
		<tr>
			<?php $imgid = get_field('member-sche4-1'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td class="memtd1">
			<p><?php the_field('member-sche4-1',$post->ID); ?></p>
			</td><?php endif;?>
			<?php $imgid = get_field('member-sche4-2'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td>
			<p><?php the_field('member-sche4-2',$post->ID); ?></p>
			</td><?php endif;?>
		</tr>
		<tr>
			<?php $imgid = get_field('member-sche5-1'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td class="memtd1">
			<p><?php the_field('member-sche5-1',$post->ID); ?></p>
			</td><?php endif;?>
			<?php $imgid = get_field('member-sche5-2'); ?>
		  	<?php if(empty($imgid)):?>
		  	<?php else:?>
			<td>
			<p><?php the_field('member-sche5-2',$post->ID); ?></p>
			</td><?php endif;?>
		</tr>
		</table>
		</div>
		  
		<?php if( get_field('member-q5') ) { ?>
		<p class="mem-q"><?php the_field('member-q5'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-a5') ) { ?>
		<p><?php the_field('member-a5'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-q6') ) { ?>
		<p class="mem-q"><?php the_field('member-q6'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-a6') ) { ?>
		<p><?php the_field('member-a6'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-q7') ) { ?>
		<p class="mem-q"><?php the_field('member-q7'); ?></p>
		<?php } ?>
		  
		<?php if( get_field('member-subimg2') ) { ?>
	  	<?php $imgid = get_field('member-subimg2');
		$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
	  	<div class="mem-subimg"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></div>
		<?php } ?>
		  
		<?php if( get_field('member-a7') ) { ?>
		<p><?php the_field('member-a7'); ?></p>
		<?php } ?>
		  
		</div>

<?php if( get_field('member-bn-link') ):?>
<a href="<?php the_field('member-bn-link'); ?>" target="_blank">
		<?php if( get_field('member-bn') ) { ?>
	  	<?php $imgid = get_field('member-bn');
		$img = wp_get_attachment_image_src( $imgid , 'full' ); ?>
	  	<p class="center"><img src="<?php echo $img[0]; ?>" width="<?php echo $img[1]; ?>" height="<?php echo $img[2]; ?>" alt="<?php the_title_attribute(); ?>"></p>
		<?php } ?>
</a>
<?php endif; ?>
  
<h3 id="ancher-entry" class="entrytitle"><img src="/wp-content/themes/welks/images/re_entrytitle.png" alt="ENTRY"></h3>
<p class="center margin-t40">あなたらしく働ける場所が、<br class="br-sp">WELKSできっと見つかる</p>

<ul class="box-w960">
    <li>
    <a href="/recruit/newgraduates/"><img src="/wp-content/themes/welks/images/re_btn_ent_new.png" alt="新卒採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_new_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_new.png'"></a>
    </li>
    <li>
	<a href="/recruit/career/"><img src="/wp-content/themes/welks/images/re_btn_ent_car.png" alt="中途採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_car_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_car.png'"></a>
	    </li>
</ul>

<ul class="box-w960b">
    <li>
    <a href="/recruit/pwd/"><img src="/wp-content/themes/welks/images/re_btn_ent_pwd.png" alt="障害者採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_pwd_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_pwd.png'"></a>
    </li>
    <li>
    <a href="/recruit/ptj/"><img src="/wp-content/themes/welks/images/re_btn_ent_ptj.png" alt="パート・アルバイト" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_ptj_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_ptj.png'"></a>
</ul>
		  
<?php the_content(); ?>
    <div class="prtable"><iframe frameborder="0" style="width:100%;height:500px;" src="https://www.pr-table.com/embed/welks"></iframe></div>
<?php endwhile; endif; ?>
		  
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php get_footer("4"); ?>