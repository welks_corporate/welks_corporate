<?php get_header(); ?>

<div class="bg">

<div class="box1">
<h2 class="errortitle">404 Not Found</h2>
<p class="txt2">お探しのページが見つかりませんでした。<br />一時的にアクセス出来ない状況にあるか、移動・削除された可能性があります。</p>
<p class="txt2"><a href="/">TOPへ戻る</a></p>
</div>

</div>

<?php get_footer(); ?>
