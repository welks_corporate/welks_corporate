<?php
/*
Template Name:採用情報（背景グレー）
*/
?>

<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header("4"); ?>

<div id="main-content" class="main-content1">

<h2 class="pagetitle2"><?php the_title(); ?></h2>
  
	<div id="primary" class="content-area2">
		<div id="content" class="site-content" role="main">
		  
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<?php the_content(); ?>
<?php endwhile; endif; ?>
		  
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->
		  
<h3 id="ancher-entry" class="entrytitle"><img src="/wp-content/themes/welks/images/re_entrytitle.png" alt="ENTRY"></h3>
<p class="center margin-t40">あなたらしく働ける場所が、<br class="br-sp">WELKSできっと見つかる</p>

<ul class="box-w960">
    <li>
    <a href="/recruit/newgraduates/"><img src="/wp-content/themes/welks/images/re_btn_ent_new.png" alt="新卒採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_new_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_new.png'"></a>
    </li>
    <li>
	<a href="/recruit/career/"><img src="/wp-content/themes/welks/images/re_btn_ent_car.png" alt="中途採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_car_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_car.png'"></a>
	    </li>
</ul>

<ul class="box-w960b">
    <li>
    <a href="/recruit/pwd/"><img src="/wp-content/themes/welks/images/re_btn_ent_pwd.png" alt="障害者採用" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_pwd_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_pwd.png'"></a>
    </li>
    <li>
    <a href="/recruit/ptj/"><img src="/wp-content/themes/welks/images/re_btn_ent_ptj.png" alt="パート・アルバイト" onmouseover="this.src='/wp-content/themes/welks/images/re_btn_ent_ptj_af.png'" onmouseout="this.src='/wp-content/themes/welks/images/re_btn_ent_ptj.png'"></a>
</ul>

<?php get_footer("4"); ?>