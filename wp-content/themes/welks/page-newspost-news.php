<?php
/*
Template Name:　NEWS：ニュースリリース（リニューアル）
*/
?>

<?php get_header(); ?>

<div class="bg">

<h1 class="h1">NEWS<p>【ニュース】</p></h1>

<ul class="breadcrumb">
  <li><a href="/">TOP</a></li>
  <li>NEWS</li>
</ul>

<ul class="box10">
  <li class="box5-2"><a href="/news/">全て</a></li>
  <li class="box5-1">ニュースリリース</li>
  <li class="box5-2"><a href="/news/report/">調査レポート</a></li>
  <li class="box5-2"><a href="/news/media/">メディア掲載</a></li>
  <li class="box5-2"><a href="/news/event/">イベント情報</a></li>
</ul>

<div class="box1">
<ul class="news-bg">
  <?php
      query_posts("&posts_per_page=15&cat=22&paged=$paged");
      if (have_posts()) :
      while ( have_posts() ) : the_post();
  ?>
  <li class="news">
    <div class="news-thumnail">
      <?php if(has_post_thumbnail()): ?>
          <?php twentyfourteen_post_thumbnail(); ?>
      <?php else: ?>
          <img src="<?php echo get_template_directory_uri(); ?>/images/noimage.png" alt="no image">
      <?php endif; ?>
    </div>
    <div class="news-txt"><p class="p1"><span class="gray txt12"><?php echo get_the_date(); ?></span><span class="newsrelease"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></span></p>
    <h2 class="txt22 bold"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2></div>
  </li>
  <?php endwhile; endif; ?>
</ul>
</div>

</div>

<?php get_footer(); ?>
