<ul class="news-bg">
  <?php
      query_posts("&posts_per_page=3&cat=15&paged=$paged");
      if (have_posts()) :
      while ( have_posts() ) : the_post();
  ?>
  <li class="news1">
    <div class="news-date"><?php echo get_the_date(); ?></div>
    <div class="news-txt1"><a href="<?php the_permalink();?>"><?php the_title();?></a></div>
  </li>
  <?php endwhile; endif; ?>
</ul>
