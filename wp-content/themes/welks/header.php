<?php ?>

<!DOCTYPE html>

<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="shortcut icon" href="https://welks.co.jp/wp-content/themes/welks/images/favicon.ico" type="image/ico" />
<link rel="profile" href="https://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://welks.co.jp/wp-content/themes/welks/style2.css">

<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>

<!-- B-Dash Tag -->
<script type="text/javascript" src="//analytics.fs-bdash.com/5A3T0E/bd-5A3T0E-1.js" charset="UTF-8"></script>
<!-- End B-Dash Tag -->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '305178122992284');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=305178122992284&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-61983917-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-61983917-1');
</script>

</head>
<body class="drawer drawer--right">

	<header>

    <nav class="pc">
      <ul>
        <li class="pc"><a href="/"><img src="<?php bloginfo('template_url'); ?>/images/logo.png"></a></li>
        <li><a href="/vision/">VISION</a></li>
        <li><a href="/service/">SERVICE</a></li>
        <li><a href="/news/">NEWS</a></li>
        <li><a href="/recruit/">RECRUIT</a></li>
        <li><a href="/about/">ABOUT</a></li>
        <li class="sp"><a href="/environment/">ENVIRONMENT</a></li>
        <li class="sp"><a href="/interview/">INTERVIEW</a></li>
        <li class="sp"><a href="/data/">DATA</a></li>
        <li><a href="/contact/">CONTACT</a></li>
      </ul>
    </nav>

    <div id="nav-drawer" class="sp">
      <a href="/"><img src="<?php bloginfo('template_url'); ?>/images/logo.png" width="100" class="sp-logo"></a>
        <input id="nav-input" type="checkbox" class="nav-unshown">
        <label id="nav-open" for="nav-input"><span></span></label>
        <label class="nav-unshown" id="nav-close" for="nav-input"></label>
        <div id="nav-content">
          <ul class="spmenu-ul">
            <li><a href="/vision/">VISION</a></li>
            <li><a href="/service/">SERVICE</a></li>
            <li><a href="/news/">NEWS</a></li>
            <li><a href="/recruit/">RECRUIT</a></li>
            <li><a href="/about/">ABOUT</a></li>
            <li><a href="/environment/">ENVIRONMENT</a></li>
            <li><a href="/interview/">INTERVIEW</a></li>
            <li><a href="/data/">DATA</a></li>
            <li><a href="/contact/">CONTACT</a></li>
          </ul>
        </div>
    </div>

	</header>
