<h3 class="midashi3">沿革</h3>

<table class="tb-enkaku">
<tr>
<td class="td-enkaku1">2013年4月23日</td>
<td class="td-enkaku2">東京都墨田区にて株式会社ウェルクスを設立</td>
</tr>
<tr>
<td class="td-enkaku1">2013年7月1日</td>
<td class="td-enkaku2">保育士・幼稚園教諭専門の転職支援サービス「保育のお仕事」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2013年7月1日</td>
<td class="td-enkaku2">保育士・幼稚園教諭専門の求人広告サービス「保育士求人ナビ」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2014年1月20日</td>
<td class="td-enkaku2">保育園・幼稚園の採用支援サービス「保育採用の窓口」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2014年4月1日</td>
<td class="td-enkaku2">治療家専門の転職支援サービス「治療家のお仕事」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2014年8月1日</td>
<td class="td-enkaku2">訪問医療マッサージサービス「ウェルクス墨田」開始（2016年11月30日売却）</td>
</tr>
<tr>
<td class="td-enkaku1">2014年9月1日</td>
<td class="td-enkaku2">介護業界専門の転職支援サービス「介護のお仕事」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2015年1月1日</td>
<td class="td-enkaku2">栄養士・管理栄養士専門の転職支援サービス「栄養士のお仕事」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2015年4月1日</td>
<td class="td-enkaku2">保育士の情報サイト「ほいくみー」開始（現：ほいくらいふ）</td>
</tr>
<tr>
<td class="td-enkaku1">2015年5月17日</td>
<td class="td-enkaku2">認知症に特化したWEBマガジン「認知症ONLINE」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2015年6月4日</td>
<td class="td-enkaku2">来春就職予定の保育学生を対象とした就職情報サイト「保育士就職ナビ」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2015年7月1日</td>
<td class="td-enkaku2">放課後等デイサービス「STEP」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2015年8月1日</td>
<td class="td-enkaku2">事業拡大に伴い本社オフィス移転</td>
</tr>
<tr>
<td class="td-enkaku1">2016年4月1日</td>
<td class="td-enkaku2">PT・OT・ST専門の転職支援サイト「リハビリのお仕事」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2016年4月1日</td>
<td class="td-enkaku2">女性のキャリアアップ資格講座の資料請求サイト「ほいくみー資格」開始（現：WOMORE）</td>
</tr>
<tr>
<td class="td-enkaku1">2016年5月9日</td>
<td class="td-enkaku2">全国の学童・アフタースクールの検索サイト「ほいくみー放課後ナビ」開始（現：ほいくらいふ放課後ナビ）</td>
</tr>
<tr>
<td class="td-enkaku1">2016年8月1日</td>
<td class="td-enkaku2">調理師の転職支援サービス「調理のお仕事」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2016年12月14日</td>
<td class="td-enkaku2">障害者雇用に特化した求人サイト「凸凹ナビ」開始</td>
</tr>
<tr>
<td class="td-enkaku1">2017年3月9日</td>
<td class="td-enkaku2">保育園の業務支援、ICT導入支援サービス比較・資料請求サイト「保育園支援ナビ」開始</td>
</tr>
<td class="td-enkaku1">2017年5月23日</td>
<td class="td-enkaku2">ベビーシッターの求人広告サービス「シッター求人ナビ」開始</td>
</tr>
<td class="td-enkaku1">2017年6月1日</td>
<td class="td-enkaku2">障害者施設専門の求人サイト「障害者支援求人ナビ」開始</td>
</tr>
<td class="td-enkaku1">2017年6月1日</td>
<td class="td-enkaku2">介護ロボットの情報が集まるWebマガジン「介護ロボットONLINE」開始</td>
<tr>
<td class="td-enkaku1">2017年8月1日</td>
<td class="td-enkaku2">福岡事業所を開設</td>
</tr>
</tr>
</table>