<?php ?>

<footer>

<p class="center"><img src="<?php bloginfo('template_url'); ?>/images/logo_footer.png"></p>

  <div class="footer-menu1">
    <dl>
      <dt><a href="/vision/">VISION</a></dt>
      <dd><a href="/vision#service-mission/">ミッション</a></dd>
      <dd><a href="/vision#service-kigyorinen/">企業理念</a></dd>
      <dd><a href="/vision#service-jinzairinen/">人材理念</a></dd>
      <dd><a href="/vision#service-spirit/">スピリット</a></dd>
    </dl>
    <dl>
      <dt><a href="/service/">SERVICE</a></dt>
      <dd><a href="/service/hoiku/">保育</a></dd>
      <dd><a href="/service/kaigo/">介護</a></dd>
      <dd><a href="/service/fukushi/">福祉</a></dd>
      <dd><a href="/service/manabi">学び</a></dd>
    </dl>
    <dl>
      <dt><a href="/news/">NEWS</a></dt>
      <dd><a href="/news/newsrelease/">ニュースリリース</a></dd>
      <dd><a href="/news/report/">調査レポート</a></dd>
      <dd><a href="/news/media/">メディア掲載</a></dd>
      <dd><a href="/news/event/">イベント情報</a></dd>
    </dl>
    <dl>
      <dt><a href="/recruit/">RECRUIT</a></dt>
      <dd><a href="/recruit#recruit-new/">新卒採用</a></dd>
      <dd><a href="/recruit#recruit-career/">中途採用</a></dd>
      <dd><a href="/recruit#recruit-ptj/">パート採用</a></dd>
      <dd><a href="https://houkago-step.com/recruit/">STEP</a></dd>
    </dl>
    <dl>
      <dt><a href="/about/">ABOUT</a></dt>
      <dd><a href="/about/message/">代表挨拶</a></dd>
      <dd><a href="/about/company/">会社概要</a></dd>
    </dl>
    <dl>
      <dt><a href="/environment/">ENVIRONMENT</a></dt>
      <dd><a href="/environment/taskforce/">タスクフォース</a></dd>
      <dt class="pc"><a href="/interview/">INTERVIEW</a></dt>
      <dt class="pc"><a href="/data/">DATA</a></dt>
    </dl>
    <dl class="sp">
      <dt><a href="/interview/">INTERVIEW</a></dt>
    </dl>
    <dl class="sp">
      <dt><a href="/data/">DATA</a></dt>
    </dl>
    <dl>
      <dt><a href="/contact/">CONTACT</a></dt>
      <dd><a href="/contact/form1/">報道関係</a></dd>
      <dd><a href="/contact/form2/">サービス退会</a></dd>
      <dd><a href="/contact/form3/">その他</a></dd>
    </dl>
  </div>

<ul class="footer-menu2">
  <li><a href="/privacy/">個人情報保護方針</a></li>
  <li><a href="/questionnaire/">アンケート調査実施に関する規約</a></li>
  <li>©2013-2018 WELKS Co.,Ltd.</li>
</ul>

</footer>

	<?php wp_footer(); ?>

<!-- ソーシャルメディアボタン -->
<script>(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.0";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<script>
window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
</script>
<!-- ソーシャルメディアボタン end -->

</body>
</html>
