<?php get_header(); ?>

<div class="top_bg">
  <div class="top_txt">
    <h2><img src="<?php bloginfo('template_url'); ?>/images/top_title.png"></h2>
    <p class="pc">環境や境遇に左右されない<br>誰もが自己実現のチャンスを得られる社会を目指します</p>
  </div>
</div>

<div class="top_hoiku_bg">
  <div class="top_hoiku_txt">
    <h3 class="txt80">保育<h3>
    <p class="pc">保育に関わる人がイキイキと活躍する社会を目指しています。</p>
    <p class="pc">2017年10月の保育士の有効求人倍率（※）は<br>全国で2.76、東京で5.99と深刻な保育士不足です。<br>保育士不足は待機児童など大きな影響を与えています。<br>
    <span class="txt8">※厚生労働省</span></p>
    <p class="pc">私たちは、保育士の転職支援やコミュニティサイトの運営により<br>保育業界の問題解決を目指しています。</p>
    <p class="sp">私たちは、保育士の転職支援やコミュニティサイトの運営により保育業界の問題解決を目指しています。</p>
  </div>
</div>
<div class="top_kaigo_bg">
  <div class="top_kaigo_txt">
    <h3 class="txt80">介護<h3>
    <p class="pc">安心して老後を迎えられる社会の実現を目指しています。</p>
    <p class="pc">厚生労働省は2025年に介護職が約38万人不足すると予測しています。</p>
    <p class="pc">私たちは、介護職の転職支援や介護に関するメディア運営により<br>介護業界の問題解決を目指しています。</p>
    <p class="sp">私たちは、介護職の転職支援や介護に関するメディア運営により介護業界の問題解決を目指しています。</p>
  </div>
</div>
<div class="top_fukushi_bg">
  <div class="top_fukushi_txt">
    <h3 class="txt80">福祉<h3>
    <p class="pc">医療・福祉分野では、リハビリ職の転職支援、<br>障害者支援施設の求人サイト、放課後等デイサービスを運営しています。</p>
    <p class="pc">私たちは、放課後等デイサービスの運営を通して、<br>特性や個性を持つ子どもの自立を支援しています。</p>
    <p class="sp">私たちは、放課後等デイサービスの運営を通して、特性や個性を持つ子どもの自立を支援しています。</p>
  </div>
</div>
<div class="top_manabi_bg">
  <div class="top_manabi_txt">
    <h3 class="txt80">学び<h3>
    <p class="pc">2016年の15～64歳の女性の就業率は66.0%（※）となり、<br>1968年の調査開始以来、過去最高を更新しました。<br>
    <span class="txt8">※2017年：男女共同参画白書</span></p>
    <p class="pc">私たちは、女性の自己実現をサポートするために<br>スキルやキャリアアップに役立つ情報サイトを運営しています。</p>
    <p class="sp">私たちは、女性の自己実現をサポートするために<br>スキルやキャリアアップに役立つ情報サイトを運営しています。</p>
  </div>
</div>

<div class="box1">
<h4 class="txt32">NEWS</h4>
<ul class="news-bg">
  <?php
      query_posts("&posts_per_page=5&cat=20,22,17,21&paged=$paged");
      if (have_posts()) :
      while ( have_posts() ) : the_post();
  ?>
  <li class="news1">
    <div class="news-date"><span class="gray txt12"><?php echo get_the_date(); ?></span></div>
    <div class="news-txt1"><span class="<?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->slug; } ?>"><?php $cat = get_the_category(); $cat = $cat[0]; { echo $cat->cat_name; } ?></span><a href="<?php the_permalink();?>"><?php the_title();?></a></div>
  <li>
  <?php endwhile; endif; ?>
</ul>
  <button class="btn2"><a href="/news/">more</a></button>
</div>

<?php get_footer(); ?>
